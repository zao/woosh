solution "woosh"
  location "prj"
  configurations {
    "Debug",
    "Release",
  }
  
  platforms {
    "x64",
  }

  language "C++"
  startproject "woosh"

  defines {
    "_WIN32_WINNT=0x0601",
    "_CRT_SECURE_NO_DEPRECATE",
    "_CRT_SECURE_NO_WARNINGS",
    "_SCL_SECURE_NO_DEPRECATE",
    "_SCL_SECURE_NO_WARNINGS",
    "UNICODE", "_UNICODE",
  }
  local dep_dir = "X:/libs/vc14-x64-release/"
  local glew_dll = path.join(dep_dir, "bin/glew32.dll")
  local glfw_dll = path.join(dep_dir, "bin/glfw3.dll")
  local run_dir = path.join(solution().basedir, "run/")

  buildoptions { "/wd4005", "/wd4996" }

  includedirs {
    "./src",
    dep_dir .. "include"
  }
  
  libdirs {
    dep_dir .. "lib"
  }

  flags {
    -- "ExtraWarnings",
    "MinimumWarnings",
    "NoEditAndContinue",
    "NoImportLib",
    "NoIncrementalLink",
    "StaticRuntime",
    "Symbols",
  }

  linkoptions {
    "/DEBUG:FULL",
  }

  targetdir "bin"
  objdir "obj"

project "woosh"
  kind "WindowedApp"
  uuid "CB236FF8-5DCB-4E4F-A9B0-36E88C30E76A"

  targetdir "run"

  links { "glfw3dll", "glew32", "opengl32", "shlwapi", }

  excludes {}

  files {
    "src/**.c",
    "src/**.cc",
    "src/**.h",
  }

  postbuildcommands {
    "copy " .. path.translate(glew_dll) .. " " .. path.translate(run_dir),
    "copy " .. path.translate(glfw_dll) .. " " .. path.translate(run_dir)
  }

  configuration "Debug"
    links {}

  configuration "Release"
    links {}
