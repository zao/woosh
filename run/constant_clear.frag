#version 450 core

layout(location = 0) uniform vec4 u_clear_color;
layout(location = 0) out vec4 out_color;

void main() {
    out_color = u_clear_color;
}