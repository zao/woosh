#version 450 core

layout(location = 0) out vec2 v_texcoord;

void main() {
    gl_Position =
        gl_VertexID == 0 ? vec4(-1.0, -1.0, 0.0, 1.0) :
        gl_VertexID == 1 ? vec4(+3.0, -1.0, 0.0, 1.0) :
        vec4(-1.0, +3.0, 0.0, 1.0);
    v_texcoord =
        gl_VertexID == 0 ? vec2(0.0, 0.0) :
        gl_VertexID == 1 ? vec2(2.0, 0.0) :
        vec2(0.0, 2.0);
}