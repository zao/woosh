#version 450 core

layout(location = 0) uniform sampler2D u_diffuse_tex;

layout(location = 0) in vec2 v_texcoord;

layout(location = 0) out vec4 out_color;

void main() {
    out_color = texture(u_diffuse_tex, v_texcoord);
}