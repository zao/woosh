#version 450 core

layout(location = 1) uniform mat4 u_xform;

layout(location = 0) in vec2 i_position;
layout(location = 1) in vec2 i_texcoord;

layout(location = 0) out vec2 v_texcoord;

void main() {
    vec4 pos = 
    gl_Position = u_xform * vec4(i_position, 0.0, 1.0);
    v_texcoord = i_texcoord;
}