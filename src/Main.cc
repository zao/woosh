﻿#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <algorithm>
#include <array>
#include <cmath>
#include <fstream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat3x3.hpp>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "stb_image.h"

#ifdef _WIN32
#include <Shlwapi.h>
#include <Windows.h>
#endif

#define countof(a) (sizeof((a)) / sizeof((a)[0]))

GLFWwindow* g_wnd;

void
on_glfw_key(GLFWwindow* wnd, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_F4 && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(wnd, GLFW_TRUE);
  }
  if (key == GLFW_KEY_ENTER && action == GLFW_RELEASE && mods == GLFW_MOD_ALT) {
    GLFWmonitor* mon = glfwGetWindowMonitor(wnd);
    if (mon) {
      GLFWvidmode const* mode = glfwGetVideoMode(mon);
      glfwSetWindowMonitor(
        wnd, nullptr, 0, 0, mode->width, mode->height, mode->refreshRate);
      glfwRestoreWindow(wnd);
      glfwMaximizeWindow(wnd);
    }
    else {
      mon = glfwGetPrimaryMonitor();
      GLFWvidmode const* mode = glfwGetVideoMode(mon);
      glfwSetWindowMonitor(
        wnd, mon, 0, 0, mode->width, mode->height, mode->refreshRate);
    }
  }
}

namespace gl {
  struct BufferHandle
  {
    BufferHandle& operator=(BufferHandle&& rhs)
    {
      if (this != &rhs) {
        if (id) {
          glDeleteBuffers(1, &id);
          id = 0;
        }
        std::swap(id, rhs.id);
      }
      return *this;
    }
    ~BufferHandle()
    {
      if (id) {
        glDeleteBuffers(1, &id);
      }
      id = 0;
    }

    GLuint id = 0;
  };

  struct ProgramHandle
  {
    ProgramHandle& operator=(ProgramHandle&& rhs)
    {
      if (this != &rhs) {
        if (id) {
          glDeleteProgram(id);
          id = 0;
        }
        std::swap(id, rhs.id);
      }
      return *this;
    }
    ~ProgramHandle()
    {
      if (id) {
        glDeleteProgram(id);
      }
      id = 0;
    }

    GLuint id = 0;
  };

  struct ShaderHandle
  {
    ShaderHandle& operator=(ShaderHandle&& rhs)
    {
      if (this != &rhs) {
        if (id) {
          glDeleteShader(id);
          id = 0;
        }
        std::swap(id, rhs.id);
      }
      return *this;
    }
    ~ShaderHandle()
    {
      if (id) {
        glDeleteShader(id);
      }
      id = 0;
    }

    GLuint id = 0;
  };

  struct TextureHandle
  {
    TextureHandle& operator=(TextureHandle&& rhs)
    {
      if (this != &rhs) {
        if (id) {
          glDeleteTextures(1, &id);
          id = 0;
        }
        std::swap(id, rhs.id);
      }
      return *this;
    }
    ~TextureHandle()
    {
      if (id) {
        glDeleteTextures(1, &id);
      }
      id = 0;
    }

    GLuint id = 0;
  };

  struct VertexArrayHandle
  {
    VertexArrayHandle& operator=(VertexArrayHandle&& rhs)
    {
      if (this != &rhs) {
        if (id) {
          glDeleteVertexArrays(1, &id);
          id = 0;
        }
        std::swap(id, rhs.id);
      }
      return *this;
    }
    ~VertexArrayHandle()
    {
      if (id) {
        glDeleteVertexArrays(1, &id);
      }
      id = 0;
    }

    GLuint id = 0;
  };
}

namespace names {
  enum Shader
  {
    SHADER_VERTEX_FULL_SCENE_TRIANGLE,
    SHADER_VERTEX_UI_TRIANGLES,
    SHADER_FRAGMENT_CONSTANT_CLEAR,
    SHADER_FRAGMENT_UI_TEXTURE,
    SHADER_COUNT,
  };
  enum Program
  {
    PROGRAM_CONSTANT_CLEAR,
    PROGRAM_TEXTURED_UI,
    PROGRAM_COUNT,
  };
  enum Texture
  {
    TEXTURE_WOOSH_LOGO,
    TEXTURE_COUNT,
  };
}

std::string
get_asset_filename(names::Shader name)
{
  switch (name) {
  case names::SHADER_VERTEX_FULL_SCENE_TRIANGLE:
    return "full_scene_triangle.vert";
  case names::SHADER_VERTEX_UI_TRIANGLES:
    return "ui_triangles.vert";
  case names::SHADER_FRAGMENT_CONSTANT_CLEAR:
    return "constant_clear.frag";
  case names::SHADER_FRAGMENT_UI_TEXTURE:
    return "ui_texture.frag";
  }
  return "";
}

std::string
get_asset_filename(names::Texture name)
{
  switch (name) {
  case names::TEXTURE_WOOSH_LOGO:
    return "woosh_logo.png";
  }
  return "";
}

GLenum
get_shader_kind(names::Shader name)
{
  switch (name) {
  case names::SHADER_VERTEX_FULL_SCENE_TRIANGLE:
  case names::SHADER_VERTEX_UI_TRIANGLES:
    return GL_VERTEX_SHADER;
  case names::SHADER_FRAGMENT_CONSTANT_CLEAR:
  case names::SHADER_FRAGMENT_UI_TEXTURE:
    return GL_FRAGMENT_SHADER;
  }
  return GL_INVALID_ENUM;
}

GLenum
get_texture_kind(names::Texture name)
{
  switch (name) {
  case names::TEXTURE_WOOSH_LOGO:
    return GL_TEXTURE_2D;
  }
  return GL_INVALID_ENUM;
}

namespace assets {
  std::array<gl::ShaderHandle, names::SHADER_COUNT> shaders;
  std::array<gl::ProgramHandle, names::PROGRAM_COUNT> programs;
  std::array<gl::TextureHandle, names::TEXTURE_COUNT> textures;
}

std::string g_prog_dir;

std::string
get_prog_dir()
{
  static std::string prog_dir = []() -> std::string {
#ifdef _WIN32
    std::vector<char> name(1 << 16);
    GetModuleFileNameA(nullptr, name.data(), (DWORD)name.size());
    PathRemoveFileSpecA(name.data());
    return name.data();
#endif
  }();
  return prog_dir;
}

bool
slurp_data_file(std::string filename, std::vector<char>& out)
{
  filename = get_prog_dir() + "\\" + filename;
  std::ifstream is(filename.c_str(), std::ios::binary);
  if (!is) {
    return false;
  }
  is.seekg(0, std::ios::end);
  size_t cb = (size_t)is.tellg();
  is.seekg(0, std::ios::beg);
  out.resize(cb);
  if (!is.read(out.data(), cb)) {
    return false;
  }
  return true;
}

bool
slurp_text_file(std::string filename, std::string& out)
{
  filename = get_prog_dir() + "\\" + filename;
  std::ifstream is(filename.c_str());
  if (!is) {
    return false;
  }
  std::ostringstream oss;
  std::string line;
  while (std::getline(is, line)) {
    oss << line << std::endl;
  }
  out = oss.str();
  return true;
}

void
build_program(names::Program program_name,
  names::Shader vs_name,
  names::Shader fs_name)
{
  auto& program = assets::programs[program_name];
  auto& vs = assets::shaders[vs_name];
  auto& fs = assets::shaders[fs_name];
  program.id = glCreateProgram();
  glAttachShader(program.id, vs.id);
  glAttachShader(program.id, fs.id);
  glLinkProgram(program.id);

  GLint status, log_len;
  glGetProgramiv(program.id, GL_LINK_STATUS, &status);
  glGetProgramiv(program.id, GL_INFO_LOG_LENGTH, &log_len);

  std::string log;
  if (log_len) {
    std::vector<char> log_buf(log_len);
    glGetProgramInfoLog(program.id, log_len, &log_len, log_buf.data());
    log = log_buf.data();
  }
}

bool
build_texture(names::Texture texture_name)
{
  auto& tex = assets::textures[texture_name];
  std::string filename = get_asset_filename(texture_name);
  GLenum type = get_texture_kind(texture_name);
  glCreateTextures(type, 1, &tex.id);
  std::vector<char> data;
  if (!slurp_data_file(filename, data)) {
    abort();
    return false;
  }
  if (type == GL_TEXTURE_2D) {
    int w, h, components_in_file;
    stbi_uc* pixels = stbi_load_from_memory((stbi_uc const*)data.data(), data.size(), &w, &h, &components_in_file, 4);
    if (!pixels) {
      abort();
      return false;
    }

    size_t const line_stride = w * 4;
    auto tmp_line = std::make_unique<stbi_uc[]>(line_stride);
    for (int row = 0; row < h / 2; ++row) {
      auto* top = pixels + row * line_stride;
      auto* bot = pixels + (h - row - 1) * line_stride;
      std::memcpy(tmp_line.get(), top, line_stride);
      std::memcpy(top, bot, line_stride);
      std::memcpy(bot, tmp_line.get(), line_stride);
    }
    glTextureStorage2D(tex.id, 1, GL_SRGB8_ALPHA8, w, h);
    glTextureSubImage2D(tex.id, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    stbi_image_free(pixels);
  }
  return true;
}

void
build_shader(names::Shader shader_name)
{
  GLenum type = get_shader_kind(shader_name);
  std::string filename = get_asset_filename(shader_name);
  auto& shader = assets::shaders[shader_name];
  shader.id = glCreateShader(type);
  std::string shader_source;
  if (!slurp_text_file(filename, shader_source)) {
    abort();
  }
  {
    char const* str[1] = {
      shader_source.c_str(),
    };
    GLint len[1] = {
      (GLint)shader_source.size(),
    };
    glShaderSource(shader.id, 1, str, len);
  }
  glCompileShader(shader.id);
  GLint status, log_len;
  glGetShaderiv(shader.id, GL_COMPILE_STATUS, &status);
  glGetShaderiv(shader.id, GL_INFO_LOG_LENGTH, &log_len);

  std::string log;
  if (log_len) {
    std::vector<char> log_buf(log_len);
    glGetShaderInfoLog(shader.id, log_len, &log_len, log_buf.data());
    log = log_buf.data();
  }
}

void
load_all_assets()
{
  for (int i = 0; i < names::TEXTURE_COUNT; ++i) {
    build_texture((names::Texture)i);
  }
  for (int i = 0; i < names::SHADER_COUNT; ++i) {
    build_shader((names::Shader)i);
  }
  build_program(names::PROGRAM_CONSTANT_CLEAR,
    names::SHADER_VERTEX_FULL_SCENE_TRIANGLE,
    names::SHADER_FRAGMENT_CONSTANT_CLEAR);
  build_program(names::PROGRAM_TEXTURED_UI,
    names::SHADER_VERTEX_UI_TRIANGLES,
    names::SHADER_FRAGMENT_UI_TEXTURE);
}

struct Screen
{
  virtual ~Screen() {}
  virtual void draw(double now) {};
};

struct LogoScreen : Screen
{
  LogoScreen()
  {
    glCreateVertexArrays(1, &logo_vao_.id);

    glCreateBuffers(1, &logo_ib_.id);
    uint16_t ib_data[] = { 0, 1, 2, 2, 1, 3 };
    glNamedBufferStorage(logo_ib_.id, sizeof(ib_data), ib_data, 0);

    glCreateBuffers(1, &logo_vb_.id);
    float hw = 0.5f / 2.0f, hh = 0.5f / 2.0f;
    float vb_data[] = {
      -hw, -hh, 0.0f, 0.0f, +hw, -hh, 1.0f, 0.0f,
      -hw, +hh, 0.0f, 1.0f, +hw, +hh, 1.0f, 1.0f,
    };
    glNamedBufferStorage(logo_vb_.id, sizeof(vb_data), vb_data, 0);

    {
      auto vao = logo_vao_.id;
      glVertexArrayVertexBuffer(vao, 0, logo_vb_.id, 0, 4 * sizeof(float));
      glVertexArrayAttribBinding(vao, 0, 0);
      glVertexArrayAttribBinding(vao, 1, 0);
      glVertexArrayAttribFormat(vao, 0, 2, GL_FLOAT, GL_FALSE, 0);
      glVertexArrayAttribFormat(
        vao, 1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float));
      glVertexArrayElementBuffer(vao, logo_ib_.id);
      glEnableVertexArrayAttrib(vao, 0);
      glEnableVertexArrayAttrib(vao, 1);
    }
  }

  void draw(double now) override
  {
    if (first_frame_) {
      base_time_ = now;
      first_frame_ = false;
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    float t = (now - base_time_);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    {
      GLint old_vao;
      glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &old_vao);
      glBindVertexArray(logo_vao_.id);

      glUseProgram(assets::programs[names::PROGRAM_TEXTURED_UI].id);
      GLuint tex_unit = 0;
      glUniform1i(0, tex_unit);

      GLuint tex_id = assets::textures[names::TEXTURE_WOOSH_LOGO].id;
      glBindTextureUnit(tex_unit, tex_id);

      GLint viewport_params[4]{};
      glGetIntegerv(GL_VIEWPORT, viewport_params);
      float w = viewport_params[2] - viewport_params[0];
      float h = viewport_params[3] - viewport_params[1];
      float aspect = w / h;

      glm::mat4 proj;
      if (aspect >= 1.0f) {
        proj = glm::ortho(-aspect / 2.0f, +aspect / 2.0f, -0.5f, +0.5f);
    }
      else {
        proj = glm::ortho(
          -0.5f, +0.5f, 1.0f / -aspect / 2.0f, 1.0f / +aspect / 2.0f);
      }
      float logo_aspect = 1280.0f / 720.0f;
      glm::mat4 S = glm::scale(glm::mat4{}, glm::vec3{ logo_aspect, 1.0f, 1.0f });
      glm::mat4 T = glm::translate(
        glm::mat4{}, glm::vec3{ 0.25f * cosf(t), 0.25f * sinf(t), 0.0f });
      glUniformMatrix4fv(1, 1, GL_FALSE, glm::value_ptr(proj * T * S));

      glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);
      glBindVertexArray(old_vao);
    }
    glBlendFunc(GL_ONE, GL_ZERO);
    glDisable(GL_BLEND);
  }

  gl::VertexArrayHandle logo_vao_;
  gl::BufferHandle logo_ib_;
  gl::BufferHandle logo_vb_;
  bool first_frame_ = true;
  double base_time_;
};

void APIENTRY
on_gl_debug_output(GLenum source,
  GLenum type,
  GLuint id,
  GLenum severity,
  GLsizei length,
  GLchar const* message,
  void const* user_param)
{
  char buf[2048]{};
  snprintf(buf,
    countof(buf),
    "[gl] %s %d %d %d %d\n",
    message,
    source,
    type,
    id,
    severity);
#ifdef _WIN32
  OutputDebugStringA(buf);
#else
  fprintf(stderr, "%s", buf);
#endif
  if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) {
    abort();
  }
}

int
main()
{
  glfwInit();
  GLFWvidmode const* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
  {
    int hints[] = {
      GLFW_MAXIMIZED,
      GLFW_TRUE,
      GLFW_SRGB_CAPABLE,
      GLFW_TRUE,
      GLFW_CONTEXT_VERSION_MAJOR,
      4,
      GLFW_CONTEXT_VERSION_MINOR,
      5,
#ifdef _DEBUG
      GLFW_OPENGL_DEBUG_CONTEXT,
      GLFW_TRUE,
#else
      GLFW_OPENGL_DEBUG_CONTEXT,
      GLFW_TRUE,
#endif
      GLFW_OPENGL_PROFILE,
      GLFW_OPENGL_CORE_PROFILE,
    };
    for (int i = 0; i < countof(hints); i += 2) {
      glfwWindowHint(hints[i], hints[i + 1]);
    }
  }
  g_wnd =
    glfwCreateWindow(mode->width, mode->height, "Woosh", nullptr, nullptr);
  glfwMakeContextCurrent(g_wnd);
  glewInit();
  glDebugMessageCallback(on_gl_debug_output, nullptr);

#ifdef _DEBUG
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif
  glEnable(GL_FRAMEBUFFER_SRGB);
  glEnable(GL_DEPTH_TEST);

  int old_window_width = 0, old_window_height = 0;
  int window_width = 0, window_height = 0;
  glfwSetKeyCallback(g_wnd, &on_glfw_key);

  GLuint vao;
  glCreateVertexArrays(1, &vao);
  glBindVertexArray(vao);

  load_all_assets();
  std::unique_ptr<Screen> screen = std::make_unique<LogoScreen>();

  while (1) {
    if (glfwWindowShouldClose(g_wnd)) {
      break;
    }
    glfwPollEvents();
    {
      glfwGetFramebufferSize(g_wnd, &window_width, &window_height);
      if (window_width != old_window_width ||
        window_height != old_window_height) {
        glViewport(0, 0, window_width, window_height);
        old_window_width = window_width;
        old_window_height = window_height;
      }
    }
    double now = glfwGetTime();

    screen->draw(now);
    glfwSwapBuffers(g_wnd);
  }
}
